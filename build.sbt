name := "ScalaForTheImpatient"

version := "1.0"

scalaVersion := "2.11.12"

// https://mvnrepository.com/artifact/org.scalatest/scalatest
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.0" % Test


libraryDependencies += "junit" % "junit" % "4.11"

libraryDependencies += "org.mockito" % "mockito-all" % "1.9.5"

libraryDependencies += "commons-io" % "commons-io" % "2.4"

libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.6"

// https://mvnrepository.com/artifact/org.mockito/mockito-scala
libraryDependencies += "org.mockito" %% "mockito-scala" % "1.7.1" % Test

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.2"