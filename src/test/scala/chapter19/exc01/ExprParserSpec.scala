package chapter19.exc01

import org.scalatest.{FlatSpec, Matchers}

class ExprParserSpec extends FlatSpec with Matchers {
  "ExprParser" should "implement / and % operation" in {
    val parser = new ExprParser
    val result = parser.parseAll(parser.expr, "(((3-4*5)+2)/5)%2").get
    result shouldBe -1.0
  }
}
