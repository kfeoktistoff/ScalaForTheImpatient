package chapter19.exc03

import org.scalatest.{FlatSpec, Matchers}

class ListIntParserSpec extends FlatSpec with Matchers {
  "ListIntParser" should "parse comma separated integers and return List[Int]" in {
    val parser = new ListIntParser
    val result = parser.parseAll(parser.elem, "(1,23,-79)").get
    result shouldBe List(1, 23, -79)
  }
}
