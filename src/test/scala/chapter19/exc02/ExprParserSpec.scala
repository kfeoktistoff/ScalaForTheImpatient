package chapter19.exc02

import org.scalatest.{FlatSpec, Matchers}

class ExprParserSpec extends FlatSpec with Matchers {
  "ExprParser" should "implement ^ operator" in {
    val parser = new ExprParser
    val result = parser.parseAll(parser.expr, "4^(2^3)").get
    result shouldBe 65536
  }
}
