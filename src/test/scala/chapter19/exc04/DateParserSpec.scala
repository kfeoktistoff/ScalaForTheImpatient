package chapter19.exc04

import java.util.GregorianCalendar

import org.scalatest.{FlatSpec, Matchers}

class DateParserSpec extends FlatSpec with Matchers {
  "Date parser" should "parse dates in <date>T<time> format" in {
    val parser = new DateParser
    val result = parser.parseAll(parser.parseDate, "2019-12-19T16:30:05.000Z").get
    result shouldBe new GregorianCalendar(2019, 11, 19, 16, 30, 5).getTime
  }
}
