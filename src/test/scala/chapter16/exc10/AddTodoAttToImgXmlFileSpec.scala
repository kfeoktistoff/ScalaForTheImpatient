package chapter16.exc10

import org.mockito.ArgumentMatchersSugar
import org.mockito.Matchers._
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}

import scala.xml.Node

class AddTodoAttToImgXmlFileSpec extends FlatSpec with Matchers with MockitoSugar with ArgumentMatchersSugar {

  def helper(expected: Node, actual: Node) = {
    val todoAttAdder = mock[TodoAttAdder]
    when(todoAttAdder.loadXmlFromFile(any[String])) thenReturn expected
    doNothing().when(todoAttAdder).saveXmlFile(anyString(), any[Node])
    when(todoAttAdder.addTodoAttToImgXmlFile(anyString(), anyString())).thenCallRealMethod()
    todoAttAdder.addTodoAttToImgXmlFile("/tmp/test.xml", "")
    verify(todoAttAdder).saveXmlFile(anyString(), argThat((node: Node) => node equals actual))
  }

  "TodoAttAdder" should "add alt attribute to all img tags with missing the attribute" in {
    val expected = <xml>
      <img/>
      <img/>
    </xml>

    val actual = <xml>
      <img alt="TODO"/>
      <img alt="TODO"/>
    </xml>

    helper(expected, actual)
  }

  "TodoAttAdder" should "add respect CDATA values" in {
    val todoAttAdder = mock[TodoAttAdder]

    val expected = <xml>
      <![CDATA[
        You will see this in the document
        and can use reserved characters like
        < > & "
      ]]>
      <img/>
      <img/>
    </xml>

    val actual = <xml>
      <![CDATA[
        You will see this in the document
        and can use reserved characters like
        < > & "
      ]]>
      <img alt="TODO"/>
      <img alt="TODO"/>
    </xml>

    helper(expected, actual)
  }
}
