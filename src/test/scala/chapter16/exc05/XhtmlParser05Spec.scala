package chapter16.exc05

import org.scalatest.{FlatSpec, Matchers}

class XhtmlParser05Spec extends FlatSpec with Matchers {
  "An XhtmlParser" should "return all srs attributes of img tags" in {
    new XhtmlParser05().findAllSrcOfImgTags(<img src="123"></img> <img src="456" alt="abc"></img>) should contain only("123", "456")
  }

  "An XhtmlParser" should "return all srs attributes of img tags ignoring ones without src" in {
    new XhtmlParser05().findAllSrcOfImgTags(<img src="123"></img> <img alt="abc"></img>) should contain only "123"
  }

  "An XhtmlParser" should "ignore non img tags" in {
    new XhtmlParser05().findAllSrcOfImgTags(<someimg src="123"></someimg> <someimg alt="abc"></someimg>) shouldBe List()
  }
}