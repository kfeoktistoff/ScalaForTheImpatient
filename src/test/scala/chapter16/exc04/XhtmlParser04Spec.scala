package chapter16.exc04

import org.scalatest.{FlatSpec, Matchers}


class XhtmlParser04Spec extends FlatSpec with Matchers {
  "An XhtmlParser" should "return only img tags without alt attribute" in {
    new XhtmlParser04().findImgTagsWithoutAltAtt(<img src="123"></img ><img src = "456" alt="abc"></img>) shouldBe List(<img src="123"></img >)
  }

  "An XhtmlParser" should "return an empty list if there are no img tags without alt attribute" in {
    new XhtmlParser04().findImgTagsWithoutAltAtt(<img src="123" alt="cde"></img ><img src = "456" alt="abc"></img>) shouldBe List()
  }

  "An XhtmlParser" should "return an empty list if there are no img tags" in {
    new XhtmlParser04().findImgTagsWithoutAltAtt(<someimg src="123"></someimg ><someimg src = "456"></someimg>) shouldBe List()
  }
}
