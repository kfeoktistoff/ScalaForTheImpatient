package chapter16.exc09

import org.scalatest.{FlatSpec, Matchers}

class AddTodoAttToImgSpec extends FlatSpec with Matchers {
  "addTodoAttToImg function" should "add TODO attribute to img tags without alt attribute" in {
    addTodoAttToImg(<img/>) shouldEqual <img alt="TODO"/>
  }

  "addTodoAttToImg function" should "ignore img tags with alt attributes" in {
    addTodoAttToImg(<img alt="abc"/>) shouldEqual <img alt="abc"/>
  }

  "addTodoAttToImg function" should "handle internal tags" in {
    addTodoAttToImg(<div><img src="one"/></div>) shouldEqual <div><img alt="TODO" src="one"/></div>
  }
}