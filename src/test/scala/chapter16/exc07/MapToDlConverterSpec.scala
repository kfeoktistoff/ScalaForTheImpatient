package chapter16.exc07

import org.scalatest.{Matchers, FlatSpec}

class MapToDlConverterSpec extends FlatSpec with Matchers {
  "MapToDlConverter" should "" in {
    mapToDlConverter(Map("A" -> "1", "B" -> "2")) shouldBe <dl><dt>A</dt><dd>1</dd><dt>B</dt><dd>2</dd></dl>
  }

  "MapToDlConverter" should "return None if the map is empty" in mapToDlConverter(Map())

  "MapToDlConverter" should "return None if the map is null" in mapToDlConverter(null)
}