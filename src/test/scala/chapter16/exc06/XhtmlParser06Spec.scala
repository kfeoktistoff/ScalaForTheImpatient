package chapter16.exc06

import org.scalatest.{FlatSpec, Matchers}

import scala.xml.Text

class XhtmlParser06Spec extends FlatSpec with Matchers {
  "XhtmlParser" should "find all text -> link pairs" in {
    new XhtmlParser06().findAllHyperLinks(<a href="href1">link1</a> <a href="href2">link2</a>) should contain only("link1" -> Text("href1"), "link2" -> Text("href2"))
  }

  "XhtmlParser" should "put empty string if href attribute is not defined" in {
    new XhtmlParser06().findAllHyperLinks(<a href="href1">link1</a> <a>link2</a>) should contain only("link1" -> Text("href1"), "link2" -> Text(""))
  }

  "XhtmlParser" should "ignore non-link tags" in {
    new XhtmlParser06().findAllHyperLinks(<a1 href="href1">link1</a1> <a1 href="href2">link2</a1>) shouldBe List()
  }
}