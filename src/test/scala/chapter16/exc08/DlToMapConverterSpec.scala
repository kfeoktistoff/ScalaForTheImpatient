package chapter16.exc08

import org.scalatest.{FlatSpec, Matchers}

class DlToMapConverterSpec extends FlatSpec with Matchers {
  "DlToMapConverter" should "convert dt/dd into Map dt -> dd" in {
    DlToMapConverter(<dl><dt>A</dt><dd>1</dd><dt>B</dt><dd>2</dd></dl>) shouldBe Map("A" -> "1", "B" -> "2")
  }

  "DlToMapConverter" should "return None if the XML is null" in DlToMapConverter(null)
}