package chapter17.exc10

import org.scalatest.{FlatSpec, Matchers}

class MutablePairSpec extends FlatSpec with Matchers {
  "MutablePairSpec#swap" should "swap key and value of the pair" in {
    val pairWithSameTypes = new MutablePair(new Animal("first"), new Animal("second"))
    pairWithSameTypes.swap

    pairWithSameTypes.key.name shouldBe "second"
    pairWithSameTypes.value.name shouldBe "first"

    val pairWithDifferentTypes = new MutablePair(new Animal("first"), new Cat("second"))

    // Will not compile
    // pairWithDifferentTypes.swap
  }
}

class Animal(val name: String)

class Cat(override val name: String) extends Animal(name)