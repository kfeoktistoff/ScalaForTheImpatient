package chapter17.exc06

import org.scalatest.{FlatSpec, Matchers}

class MiddleFunctionSpec extends FlatSpec with Matchers {
  "Middle function" should "return middle character of the string" in {
    middle("world") shouldBe Some('r')
  }

  "Middle function" should "return None if input is empty" in {
    middle("") shouldBe None
  }

  "Middle function" should "return None if input is null" in {
    middle(null) shouldBe None
  }
}
