package chapter17.exc02

import org.scalatest.{FlatSpec, Matchers}

class MutablePairSpec extends FlatSpec with Matchers {
  "Swap method" should "swap key and value of its Pair object" in {
    val pair = new MutablePair(1, "abc")
    pair.swap()

    pair.key shouldBe "abc"
    pair.value shouldBe 1
  }
}
