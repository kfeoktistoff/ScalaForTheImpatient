package chapter17.exc03

import chapter17.exc01.ImmutablePair
import org.scalatest.{FlatSpec, Matchers}

class PairSpec extends FlatSpec with Matchers {
  "Swap method" should "return new Pair with swapped key and value" in {
    val pair = new ImmutablePair(1, "abc")
    val swappedPair = pair.swap()
    swappedPair.key shouldBe pair.value
    swappedPair.value shouldBe pair.key
  }
}
