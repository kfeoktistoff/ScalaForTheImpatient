package chapter17.exc01

import org.scalatest.{FlatSpec, Matchers}

class ImmutablePairSpec extends FlatSpec with Matchers {
  "Swap method" should "return new ImmutablePair with swapped key and value" in {
    val pair = new ImmutablePair(1, "abc")
    val swappedPair = pair.swap()
    swappedPair.key shouldBe pair.value
    swappedPair.value shouldBe pair.key
  }
}
