package chapter18.exc07

import org.mockito.Mockito.verify
import org.scalatest.mock.MockitoSugar
import org.scalatest.{FlatSpec, Matchers}

class TryAndCloseSpec extends FlatSpec with Matchers with MockitoSugar {
  "tryAndClose" should "invoke 'close()' method if the target method is finished successfully" in {
    val dummy = mock[Dummy]
    tryAndClose(dummy)(x => x.doNothing())
    verify(dummy).close()
  }

  "tryAndClose" should "invoke 'close()' method if the target method throws an exception" in {
    val dummy = mock[Dummy]
    tryAndClose(dummy)(x => x.throwException())
    verify(dummy).close()
  }

  class Dummy {
    def close(): Unit = {}

    def throwException(): Unit = throw new RuntimeException("")

    def doNothing(): Unit = {}
  }

}
