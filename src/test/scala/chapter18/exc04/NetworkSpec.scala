package chapter18.exc04

import org.scalatest.{FlatSpec, Matchers}

class NetworkSpec extends FlatSpec with Matchers {
  "Member" should "throw IllegalArgumentException if name is null" in {
    val network1 = new Network
    an[IllegalArgumentException] shouldBe thrownBy {
      network1.join(null)
    }
  }

  "Same members of different networks" should "not be equal" in {
    val network1 = new Network
    val network2 = new Network

    network1.join("User1")
    network2.join("User1")

    network1.members(0).equals(network2.members(0)) shouldBe false
  }

  "Same members of the same network" should "be equal" in {
    val network1 = new Network

    network1.join("User1")
    network1.join("User1")

    network1.members(0).equals(network1.members(1)) shouldBe true
  }
}
