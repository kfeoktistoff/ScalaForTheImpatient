package chapter18.exc06

import org.scalatest.{FlatSpec, Matchers}

class indexOfSpec extends FlatSpec with Matchers {
  "indexOf function" should "return closest index and 'false' if the value is not in the array" in {
    indexOf(Array(1, 2, 3, 4, 5), 6) shouldBe(5, false)
  }

  "indexOf function" should "return the index and 'true' if the value is in the array" in {
    indexOf(Array(1, 2, 3, 4, 5), 3) shouldBe(2, true)
  }
}
