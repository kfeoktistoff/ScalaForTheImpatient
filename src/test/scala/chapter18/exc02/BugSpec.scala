package chapter18.exc02

import org.scalatest.{FlatSpec, Matchers}

private class BugSpec extends FlatSpec with Matchers {
  "Bug" should "have correct move and turn methods" in {
    val bug = new Bug();
    bug.distance shouldBe 0
    bug.move(5).distance shouldBe 5
    bug.turn().distance shouldBe 5
    bug.move(3).distance shouldBe 2
  }

  new Bug move 4 and Show and Then move 6 and Show and Then turn Around move 5 and Show
}
