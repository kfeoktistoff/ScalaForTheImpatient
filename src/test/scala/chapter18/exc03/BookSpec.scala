package chapter18.exc03

import org.scalatest.{FlatSpec, Matchers}

class BookSpec extends FlatSpec with Matchers {
  "Book setters" should "set Title and Author" in {
    val book = new Book
    val title = "The Title"
    val author = "The Author"
    book set Title to title set Author to author

    book.title shouldBe title
    book.author shouldBe author
  }
}
