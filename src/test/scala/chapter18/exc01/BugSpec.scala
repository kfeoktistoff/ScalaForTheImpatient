package chapter18.exc01

import org.scalatest.{FlatSpec, Matchers}

class BugSpec extends FlatSpec with Matchers {
  "Bug" should "have correct move and turn methods" in {
    val bug = new Bug();
    bug.distance shouldBe 0
    bug.move(5).distance shouldBe 5
    bug.turn().distance shouldBe 5
    bug.move(3).distance shouldBe 2
  }

  new Bug().move(4).show().move(6).show().turn().move(5).show()
}
