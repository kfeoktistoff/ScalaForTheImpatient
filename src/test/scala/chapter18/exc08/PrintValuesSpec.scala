package chapter18.exc08

import org.scalatest.{FlatSpec, Matchers}

class PrintValuesSpec extends FlatSpec with Matchers {
  "applyToValues" should "apply function to all values of the range" in {
    applyToValues((x: Int) => x * x, 3, 6) shouldBe Vector(9, 16, 25, 36)
  }

  "applyToValues" should "return all indeces from the array within the range" in {
    applyToValues(Array(1, 1, 2, 3, 5, 8, 13, 21, 34, 55), 3, 6) shouldBe Vector(3, 5, 8, 13)
  }
}
