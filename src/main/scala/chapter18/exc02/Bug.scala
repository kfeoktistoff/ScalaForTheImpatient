package chapter18.exc02

object Show

object Then

object Around

class Bug {
  var isForward: Boolean = true;
  var distance: Int = 0;

  def show(): this.type = {
    println(distance.toString)
    this
  }

  def turn(): this.type = {
    isForward = !isForward
    this
  }

  def move(steps: Int): this.type = {
    if (isForward) distance = distance + steps else distance = distance - steps
    this
  }

  def and(obj: Show.type): this.type = {
    this.show()
  }

  def and(obj: Then.type): this.type = {
    this
  }

  def turn(obj: Around.type): this.type = {
    this.turn().turn()
  }
}
