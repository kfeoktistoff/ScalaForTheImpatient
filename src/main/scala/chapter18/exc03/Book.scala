package chapter18.exc03

object Title

object Author

class Book {
  var title: String = _
  var author: String = _
  private var useNextArgAs: Any = _

  def set(obj: Title.type): this.type = {
    useNextArgAs = obj
    this
  }

  def set(obj: Author.type): this.type = {
    useNextArgAs = obj
    this
  }

  def to(arg: String): this.type = {
    if (useNextArgAs == Title) {
      title = arg
    } else if (useNextArgAs == Author) {
      author = arg
    }
    this
  }
}
