package chapter18

package object exc08 {
  def applyToValues(f: {def apply(n: Int): Int}, from: Int, to: Int): Seq[Int] = {
    for (n <- from to to) yield f(n)
  }

  def printValues(f: {def apply(n: Int): Int}, from: Int, to: Int): Unit = {
    println(applyToValues(f, from, to))
  }
}
