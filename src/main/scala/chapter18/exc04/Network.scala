package chapter18.exc04

import scala.collection.mutable.ArrayBuffer

class Network {

  class Member(val name: String) {
    if (name == null) throw new IllegalArgumentException("Name should not be null")

    val network: Network = Network.this
    val contacts = new ArrayBuffer[Member]

    override def equals(obj: Any): Boolean = obj.isInstanceOf[Member] && name.equals(obj.asInstanceOf[Member].name) && this.network.eq(obj.asInstanceOf[Member].network)

    override def hashCode(): Int = network.hashCode + name.hashCode
  }

  val members = new ArrayBuffer[Member]

  def join(name: String): Unit = {
    val m = new Member(name)
    members += m
    m
  }
}
