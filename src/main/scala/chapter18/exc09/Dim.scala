package chapter18.exc09

abstract class Dim[T](val value: Double, val name: String) {
  this: T =>
  protected def create(v: Double): T

  def +(other: Dim[T]): T = create(value + other.value)

  override def toString: String = value + " " + name
}
