package chapter18.exc09

class Seconds(v: Double) extends Dim[Seconds](v, "s") {
  override protected def create(v: Double): Seconds = new Seconds(v)
}
