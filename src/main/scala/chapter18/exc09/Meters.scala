package chapter18.exc09

// Dim[Seconds] will not compile
class Meters(v: Double) extends Dim[Meters](v, "m") {
  override protected def create(v: Double): Meters = new Meters(v)
}
