package chapter18

package object exc07 {
  def tryAndClose[T <: {def close(): Unit}, R](obj: T)(f: T => R): R = {
    try {
      f(obj)
    }
    finally {
      obj.close()
    }
  }
}
