package chapter18.exc01

class Bug {
  var isForward: Boolean = true;
  var distance: Int = 0;

  def show(): this.type = {
    println(distance.toString)
    this
  }

  def turn(): this.type = {
    isForward = !isForward
    this
  }

  def move(steps: Int): this.type = {
    if (isForward) distance = distance + steps else distance = distance - steps
    this
  }
}
