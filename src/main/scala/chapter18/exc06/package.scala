package chapter18

import scala.collection.Searching.{Found, InsertionPoint, search}

package object exc06 {
  def indexOf(arr: Array[Int], v: Int): (Int, Boolean) = {
    type x[Int, Boolean] = (Int, Boolean)

    val matchResult: Int x Boolean = arr.search(v) match {
      case result: Found => (result.foundIndex, true)
      case result: InsertionPoint => (result.insertionPoint, false)
    }

    matchResult
  }
}
