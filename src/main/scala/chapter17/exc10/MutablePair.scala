package chapter17.exc10

class MutablePair[S, T](var key: S, var value: T) {
  def swap(implicit ev: T =:= S): Unit = {
    val tmp = key.asInstanceOf[T]
    key = value
    value = tmp
  }
}
