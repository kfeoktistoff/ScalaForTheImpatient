package chapter17.exc02

class MutablePair[T](var key: T, var value: T) {
  def swap(): Unit = {
    val tmp = key
    key = value
    value = tmp
  }
}