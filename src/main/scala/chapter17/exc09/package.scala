package chapter17

package object exc09 {

  class Pair[+T](val key: T, val value: T) {
    //  def replaceFirst(newFirst: T) = new Pair[T](newFirst, value)
    def replaceFirst[R >: T](newFirst: R): Pair[R] = new Pair[R](newFirst, value)
  }

  class NastyDoublePair(override val key: Double, override val value: Double) extends Pair[Double](key, value) {
    //  override def replaceFirst(newFirst: Double) = new NastyDoublePair(scala.math.sqrt(newFirst), value)
    override def replaceFirst[R >: Double](newFirst: R) = new NastyDoublePair(math.sqrt(key), value)
  }

  val nastyDoublePair: Pair[Any] = new NastyDoublePair(1.0, 2.0)
  nastyDoublePair.replaceFirst("hello")
}