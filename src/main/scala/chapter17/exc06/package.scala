package chapter17

package object exc06 {
  def middle[A, C](it: C)(implicit ev: C => Iterable[A]): Option[A] = {
    if (it == null || it.isEmpty) None else Some(it.toList(it.size / 2))
  }
}
