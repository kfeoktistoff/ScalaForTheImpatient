package chapter17.exc01

class ImmutablePair[T, S](val key: T, val value: S) {
  def swap(): ImmutablePair[S, T] = new ImmutablePair(value, key)

  override def toString: String = key.toString + ":" + value.toString
}
