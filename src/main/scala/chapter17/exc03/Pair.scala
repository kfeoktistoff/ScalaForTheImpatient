package chapter17.exc03

class Pair[T, S](val key: T, val value: S) {
  def swap(pair: Pair[T, S]): Pair[S, T] = new Pair(pair.value, pair.key)
}
