package chapter16.exc05

import scala.xml.NodeSeq

/**
 * Created by Kirill Feoktistov on 01.04.2019
 */

class XhtmlParser05 {
  def findAllSrcOfImgTags(xml: NodeSeq) = xml.filter(elem => elem.label == "img" && elem.attribute("src").nonEmpty).map(_.attributes.get("src").get.text).toList
}

