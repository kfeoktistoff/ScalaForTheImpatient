package chapter16

import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml._;

/**
 * Created by Kirill Feoktistov on 04.04.19
 */

package object exc09 {
  def addTodoAttToImg(xml: NodeSeq): Node = {
    val transformed = new RuleTransformer(
      new RewriteRule {
        override def transform(n: Node): Node = n match {
          case <img/> if n.attributes("alt") == null => n.asInstanceOf[Elem] % Attribute(null, "alt", "TODO", Null)
          case _ => n
        }
      }
    ).transform(xml)

    transformed.head
  }
}