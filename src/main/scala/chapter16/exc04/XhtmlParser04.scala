package chapter16.exc04

import scala.xml.NodeSeq

/**
 * Created by Kirill Feoktistov on 17.08.14
 */

class XhtmlParser04 {
  def findImgTagsWithoutAltAtt(xml: NodeSeq) = xml.filter(elem => elem.label == "img" && elem.attribute("alt").isEmpty).toList
}

