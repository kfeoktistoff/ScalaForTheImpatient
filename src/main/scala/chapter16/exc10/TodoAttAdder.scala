package chapter16.exc10

import java.io.File

import chapter16.exc09._

import scala.xml.dtd.{DocType, PublicID}
import scala.xml.parsing.ConstructingParser
import scala.xml.{Node, XML}

class TodoAttAdder {
  def addTodoAttToImgXmlFile(source: String, destination: String): Unit = {
    val root = loadXmlFromFile(source)
    val updated = addTodoAttToImg(root)
    saveXmlFile(destination, updated);
  }

  def loadXmlFromFile(filePath: String): Node = {
    val parser = ConstructingParser.fromFile(new File(filePath), preserveWS = true)
    val doc = parser.document()
    doc.docElem
  }

  def saveXmlFile(filePath: String, xml: Node): Unit = XML.save(filename = filePath, node = xml, enc = "UTF-8", doctype = DocType("html",
    PublicID("-//W3C//DTD XHTML 1.0 Strict//EN",
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"),
    Nil))
}
