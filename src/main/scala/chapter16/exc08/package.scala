package chapter16

import scala.xml.NodeSeq

/**
 * Created by Kirill Feoktistov on 02.04.19
 */

package object exc08 {
  def DlToMapConverter(xml: NodeSeq): Map[String, String] = {
    if (xml == null || xml.isEmpty) Map()
    else ((xml \ "dt").map(_.text) zip (xml \ "dd").map(_.text)).toMap
  }
}