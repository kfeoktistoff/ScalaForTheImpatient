package chapter16.exc06

import scala.xml.{Text, NodeSeq}

/**
 * Created by Kirill Feoktistov on 17.08.14
 */

class XhtmlParser06 {
  def findAllHyperLinks(xml: NodeSeq) = xml.filter(_.label == "a").map(elem => elem.text -> elem.attributes.get("href").getOrElse(Text(""))).toList
}
