package chapter16

/**
 * Created by Kirill Feoktistov on 02.04.19
 */

package object exc07 {
  def mapToDlConverter(map: Map[String, String]) = {
    if (map == null || map.isEmpty) None else
      <dl>{for ((k, v) <- map) yield <dt>{k}</dt><dd>{v}</dd>}</dl>
  }
}