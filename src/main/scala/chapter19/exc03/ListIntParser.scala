package chapter19.exc03

import scala.util.matching.Regex
import scala.util.parsing.combinator.RegexParsers

class ListIntParser extends RegexParsers {
  val number: Regex = "-?[0-9]+".r

  val elem: Parser[List[Int]] = "(" ~> repsep(number, ",") <~ ")" ^^ (l => l.map(_.toInt))
}
