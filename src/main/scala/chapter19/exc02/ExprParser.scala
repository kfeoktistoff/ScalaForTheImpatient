package chapter19.exc02

import scala.util.matching.Regex
import scala.util.parsing.combinator.RegexParsers

class ExprParser extends RegexParsers {
  val number: Regex = "[0-9.]+".r

  def expr: Parser[Double] = term ~ opt(("+" | "-") ~ expr) ^^ {
    case t ~ None => t
    case t ~ Some("+" ~ e) => t + e
    case t ~ Some("-" ~ e) => t - e
  }

  def term: Parser[Double] = pow ~ opt(("/" | "*" | "%") ~ term) ^^ {
    case f ~ None => f
    case f ~ Some("/" ~ e) => f / e
    case f ~ Some("*" ~ e) => f * e
    case f ~ Some("%" ~ e) => f % e
  }

  def pow: Parser[Double] = factor ~ opt("^" ~ pow) ^^ {
    case f ~ None => f
    case f ~ Some("^" ~ p) => Math.pow(f, p)
  }

  def factor: Parser[Double] = number ^^ {
    _.toDouble
  } | "(" ~> expr <~ ")"
}
