package chapter19.exc04

import java.util.{Date, GregorianCalendar}

import scala.util.matching.Regex
import scala.util.parsing.combinator.RegexParsers

class DateParser extends RegexParsers {
  val twoDigitsRx: Regex = """\d{4}""".r
  val fourDigitsRx: Regex = """\d{2}""".r
  val threeDigitsRx: Regex = """\d{3}""".r

  def parseDate: Parser[Date] =
    fourDidits ~ ("-" ~> twoDigits) ~ ("-" ~> twoDigits) ~ ("T" ~> twoDigits) ~ (":" ~> twoDigits) ~ (":" ~> twoDigits) ~ ("." ~> threeDigits ~> "Z") ^^ {
      case y ~ mo ~ d ~ h ~ mi ~ s ~ ms => (new GregorianCalendar(y, mo - 1, d, h, mi, s)).getTime
    }

  def fourDidits: Parser[Int] = twoDigitsRx ^^ {
    _.toInt
  }

  def twoDigits: Parser[Int] = fourDigitsRx ^^ {
    _.toInt
  }

  def threeDigits: Parser[Int] = threeDigitsRx ^^ {
    _.toInt
  }
}
