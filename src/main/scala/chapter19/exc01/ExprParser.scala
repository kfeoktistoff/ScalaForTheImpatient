package chapter19.exc01


import scala.util.matching.Regex
import scala.util.parsing.combinator.RegexParsers

class ExprParser extends RegexParsers {
  val number: Regex = "[0-9.]+".r

  def expr: Parser[Double] = term ~ opt(("+" | "-") ~ expr) ^^ {
    case t ~ None => t
    case t ~ Some("+" ~ e) => t + e
    case t ~ Some("-" ~ e) => t - e
    case f ~ Some("/" ~ e) => f / e
    case f ~ Some("*" ~ e) => f * e
    case f ~ Some("%" ~ e) => f % e
  }

  def term: Parser[Double] = factor ~ opt(("/" | "*" | "%") ~ term) ^^ {
    case f ~ None => f
    case f ~ Some("/" ~ e) => f / e
    case f ~ Some("*" ~ e) => f * e
    case f ~ Some("%" ~ e) => f % e
  }

  def factor: Parser[Double] = number ^^ {
    _.toDouble
  } | "(" ~> expr <~ ")"
}
